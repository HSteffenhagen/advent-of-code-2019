module Part2
where

import qualified Data.IntMap as M
import Control.Monad.State
import qualified Data.List as List
import System.Exit(exitSuccess)
import Control.Monad(forM_)
import Text.Printf(printf)

data MachineState = MachineState
  { machineMemory :: M.IntMap Int,
    machineCursor :: Int
  }

instance Show MachineState where
  show s = unlines
    . map (\(k,v) -> show k ++ ": " ++ show v ++ if k == machineCursor s then " <-" else "")
    . M.toList
    $ machineMemory s

type MachineM = State MachineState

readMemory :: Int -> MachineM Int
readMemory address = gets $ \s -> case M.lookup address (machineMemory s) of
  Just value -> value
  Nothing    -> error 
    $ "readMemory "
    ++ show address
    ++ " ("
    ++ show s
    ++ ") : Out of bounds memory access"

writeMemory :: Int -> Int -> MachineM ()
writeMemory address value = modify $ \s -> s
  { machineMemory = M.insert address value $ machineMemory s }

readNext :: MachineM Int
readNext = state go where
  go s = (nextValue, s {machineCursor = machineCursor s + 1}) where
    nextValue = case M.lookup (machineCursor s) (machineMemory s) of
      Just value -> value
      Nothing    -> error
        $ "readNext (" ++ show s ++ "): Cursor out of bounds"

doBinOp :: (Int -> Int -> Int) -> MachineM ()
doBinOp op = do
  lhs <- readNext >>= readMemory
  rhs <- readNext >>= readMemory
  target <- readNext
  writeMemory target $ op lhs rhs

doPlus :: MachineM ()
doPlus = doBinOp (+)

doMultiply :: MachineM ()
doMultiply = doBinOp (*)

readInitialMemory :: String -> M.IntMap Int
readInitialMemory = M.fromList . zip [0..] . map read . splitOn ',' where
  splitOn sep = List.unfoldr nextWord where
    nextWord [] = Nothing
    nextWord str = let (word, rest) = break (== ',') str
      in Just (word, dropWhile (==',') rest)

runToHalt :: MachineM ()
runToHalt = do
  opCode <- readNext
  case opCode of
    1 -> doPlus >> runToHalt
    2 -> doMultiply >> runToHalt
    99 -> return ()
    other -> get >>= (\s -> error
      $ "runToHalt: unknown opcode " ++ show opCode
      ++ "; " ++ show s)

evaluateInput :: Int -> Int -> M.IntMap Int -> Int
evaluateInput x y initialMemory =
  let initialState = MachineState
        { machineCursor = 0
        , machineMemory = initialMemory
        }
  in flip evalState initialState $ do
    writeMemory 1 x
    writeMemory 2 y
    runToHalt
    readMemory 0 

main :: IO ()
main = do
  initialMemory <- readInitialMemory <$> getLine
  let targetOutput = 19690720
  forM_ [(x,y) | x <- [0..99], y <- [0..99]] $ \(x,y) -> do
    let output = evaluateInput x y initialMemory
    if output == targetOutput
      then do
        printf "noun: %d, verb: %d, answer: %d\n" x y (x*100+y)
        exitSuccess
      else return ()
  putStrLn "Didn't find noun/verb combo"
