module Part1 where

import System.Environment(getArgs)

isSorted :: Ord a => [a] -> Bool
isSorted (x:y:xs) = x <= y && isSorted (y:xs)
isSorted _ = True

hasDouble :: Eq a => [a] -> Bool
hasDouble (x:y:xs) = x == y || hasDouble (y:xs)
hasDouble _ = False

possiblePasswordsBetween :: Int -> Int -> [Int]
possiblePasswordsBetween first last = filter (\p -> let ps = show p in isSorted ps && hasDouble ps) [first..last]

main :: IO ()
main = do
  [first, last] <- map read <$> getArgs
  print . length $ possiblePasswordsBetween first last
