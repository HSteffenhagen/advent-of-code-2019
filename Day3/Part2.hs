{-# LANGUAGE TupleSections #-}
module Part2 where

import Data.Ord (comparing)
import qualified Data.Set as S
import qualified Data.List as List
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Control.Applicative((<|>))

data Direction = U | D | L | R
  deriving Show
data Movement = Movement Direction Int
  deriving Show

parseMovement :: String -> Movement
parseMovement (d:l) = Movement dir (read l) where
  dir = case d of
    'U' -> U
    'D' -> D
    'L' -> L
    'R' -> R

type PositionDistances = M.Map Position Int
data Position = Position !Int !Int deriving (Eq, Show, Ord)

-- | Given movement and a starting positions, give all positions visited in between (including the final position)
applyMovement :: Movement -> (Position, Int) -> [(Position, Int)]
applyMovement (Movement dir len) ((Position x y), d0) = map (\(dx, dy, l) -> (Position (x + dx) (y + dy), d0 + l)) ds where
  ds = case dir of
    U -> map (\l -> (0, l, abs l)) [1 .. len]
    D -> map (\l -> (0, l, abs l)) [-1, -2 .. -len]
    L -> map (\l -> (l, 0, abs l)) [-1, -2 .. -len]
    R -> map (\l -> (l, 0, abs l)) [1 .. len]

-- | Given a list of new positions and a set of known positions, add those posiions to the known positions and give the final position
addPositions :: [(Position, Int)] -> PositionDistances -> ((Position, Int), PositionDistances)
addPositions newPositions knownPositions = List.foldl' (\(_,kp') posD@(pos, l) -> (posD, M.alter (<|> Just l) pos kp')) (undefined, knownPositions) newPositions

collectPoints :: [Movement] -> M.Map Position Int
collectPoints = snd . List.foldl'
  (\(p, kp) m -> addPositions (applyMovement m p) kp)
  ((Position 0 0, 0), M.empty)

-- | Split list by separator. Multiple consecutive separators are ignored.
splitOn :: Eq a => a -> [a] -> [[a]]
splitOn sep xs = List.unfoldr nextSplit xs where
  nextSplit [] = Nothing
  nextSplit rest = let (word, rest') = break (== sep) rest in
   Just (word, dropWhile (== sep) rest')

parseInputLine :: String -> [Movement]
parseInputLine = map parseMovement . splitOn ','

getInputLine :: IO [Movement]
getInputLine = fmap parseInputLine getLine


main :: IO ()
main = do
  wireOneMovements <- getInputLine
  wireTwoMovements <- getInputLine 
  let wireOnePointsToDistance = collectPoints wireOneMovements
      wireTwoPointsToDistance = collectPoints wireTwoMovements
      wireOnePoints = M.keysSet wireOnePointsToDistance
      wireTwoPoints = M.keysSet wireTwoPointsToDistance
      wireLength p m = fromMaybe (error "assuming point exists") (M.lookup p m)
      wireLengthSum p = sum $ map (wireLength p)
        [wireOnePointsToDistance, wireTwoPointsToDistance]
  print
    . (\p -> (p, wireLengthSum p))
    . List.minimumBy (comparing wireLengthSum)
    . S.toList
    $ S.intersection wireOnePoints wireTwoPoints
