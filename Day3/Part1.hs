{-# LANGUAGE TupleSections #-}
module Part1 where

import Data.Ord (comparing)
import qualified Data.Set as S
import qualified Data.List as List

data Direction = U | D | L | R
  deriving Show
data Movement = Movement Direction Int
  deriving Show

parseMovement :: String -> Movement
parseMovement (d:l) = Movement dir (read l) where
  dir = case d of
    'U' -> U
    'D' -> D
    'L' -> L
    'R' -> R

data Position = Position !Int !Int deriving (Eq, Show, Ord)

manhattanDistanceFromCenter :: Position -> Int
manhattanDistanceFromCenter (Position x y) = abs x + abs y

-- | Given movement and a starting positions, give all positions visited in between (including the final position)
applyMovement :: Movement -> Position -> [Position]
applyMovement (Movement dir len) (Position x y) = map (\(dx, dy) -> Position (x + dx) (y + dy)) ds where
  ds = case dir of
    U -> map (0,) [1 .. len]
    D -> map (0,) [-1, -2 .. -len]
    L -> map (,0) [-1, -2 .. -len]
    R -> map (,0) [1 .. len]

-- | Given a list of new positions and a set of known positions, add those posiions to the known positions and give the final position
addPositions :: [Position] -> S.Set Position -> (Position, S.Set Position)
addPositions newPositions knownPositions = List.foldl' (\(_,kp') pos -> (pos, S.insert pos kp')) (undefined, knownPositions) newPositions

collectPoints :: [Movement] -> S.Set Position
collectPoints = snd . List.foldl'
  (\(p, kp) m -> addPositions (applyMovement m p) kp)
  (Position 0 0, S.empty)

-- | Split list by separator. Multiple consecutive separators are ignored.
splitOn :: Eq a => a -> [a] -> [[a]]
splitOn sep xs = List.unfoldr nextSplit xs where
  nextSplit [] = Nothing
  nextSplit rest = let (word, rest') = break (== sep) rest in
   Just (word, dropWhile (== sep) rest')

parseInputLine :: String -> [Movement]
parseInputLine = map parseMovement . splitOn ','

getInputLine :: IO [Movement]
getInputLine = fmap parseInputLine getLine

main :: IO ()
main = do
  wireOneMovements <- getInputLine
  wireTwoMovements <- getInputLine 
  let wireOnePoints = collectPoints wireOneMovements
      wireTwoPoints = collectPoints wireTwoMovements
  print
    . (\p -> (p, manhattanDistanceFromCenter p))
    . List.minimumBy (comparing manhattanDistanceFromCenter)
    . S.toList
    $ S.intersection wireOnePoints wireTwoPoints
