module Day1Part1 where

infixr 9 -->
f --> g = g . f

calculateFuelRequirements :: Int -> Int
calculateFuelRequirements mass =
  div mass 3 - 2


main :: IO ()
main = interact
    $ lines
  --> map
    (   read
    --> calculateFuelRequirements
    )
  --> sum
  --> show
  --> (++"\n")
