module Day1Part2 where

infixr 9 -->
f --> g = g . f

calculateFuelRequirements :: Int -> Int
calculateFuelRequirements mass =
  let initialFuelRequirements = div mass 3 - 2 in
  if initialFuelRequirements <= 0
  then 0
  else initialFuelRequirements + calculateFuelRequirements initialFuelRequirements


main :: IO ()
main = interact
    $ lines
  --> map
    (   read
    --> calculateFuelRequirements
    )
  --> sum
  --> show
  --> (++"\n")
