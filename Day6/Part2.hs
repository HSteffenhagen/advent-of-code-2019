module Part1 where

import qualified Data.Map as M
import qualified Data.Set as S
import Control.Applicative ((<|>))
import Data.Maybe (fromMaybe)
import qualified Data.List as List
import Data.Monoid (First(..))
import Data.Foldable (foldMap)

newtype OrbitObject = OrbitObject String
  deriving (Eq, Ord)

newtype OrbitGraph = OrbitGraph { adjacencies :: M.Map OrbitObject (S.Set OrbitObject) }

emptyOrbitGraph = OrbitGraph M.empty

addOrbitNeighbor :: OrbitObject -> OrbitObject -> OrbitGraph -> OrbitGraph
addOrbitNeighbor center object graph = graph
  { adjacencies =
      M.alter
        (\satellites -> fmap (S.insert object) satellites <|> Just (S.singleton object))
        center
        (adjacencies graph)
  }

orbitNeighbors :: OrbitObject -> OrbitGraph -> S.Set OrbitObject
orbitNeighbors center graph = fromMaybe S.empty . M.lookup center $ adjacencies graph

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn sep = List.unfoldr nextChunk where
  nextChunk [] = Nothing
  nextChunk xs = Just $
    let (word, rest) = List.break (==sep) xs
    in (word, List.dropWhile (==sep) rest)

readOrbitLine :: String -> OrbitGraph -> OrbitGraph
readOrbitLine line =
  let [center, satellite] = map OrbitObject $ splitOn ')' line
  in addOrbitNeighbor center satellite . addOrbitNeighbor satellite center

compose :: [(a -> a)] -> a -> a
compose = foldr (.) id

getDistanceBetween :: OrbitObject -> OrbitObject -> OrbitGraph -> Maybe Int
getDistanceBetween from' to graph = fmap (subtract 2) $ go from' from' where
  go incoming from
    | from == to = Just 0
    | otherwise = getFirst
        . fmap (+1)
        . foldMap
          (\neighbor -> First $ if neighbor == incoming then Nothing else go from neighbor)
        $ orbitNeighbors from graph

me :: OrbitObject
me = OrbitObject "YOU"

santa :: OrbitObject
santa = OrbitObject "SAN"

main :: IO ()
main = interact
  $ (++"\n")
  . show
  . getDistanceBetween me santa
  . ($ emptyOrbitGraph)
  . compose
  . map readOrbitLine
  . lines
