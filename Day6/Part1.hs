module Part1 where

import qualified Data.Map as M
import qualified Data.Set as S
import Control.Applicative ((<|>))
import Data.Maybe (fromMaybe)
import qualified Data.List as List

newtype OrbitObject = OrbitObject String
  deriving (Eq, Ord)

newtype OrbitGraph = OrbitGraph { adjacencies :: M.Map OrbitObject (S.Set OrbitObject) }

emptyOrbitGraph = OrbitGraph M.empty

addOrbit :: OrbitObject -> OrbitObject -> OrbitGraph -> OrbitGraph
addOrbit center object graph = graph
  { adjacencies =
      M.alter
        (\satellites -> fmap (S.insert object) satellites <|> Just (S.singleton object))
        center
        (adjacencies graph)
  }

satellites :: OrbitObject -> OrbitGraph -> S.Set OrbitObject
satellites center graph = fromMaybe S.empty . M.lookup center $ adjacencies graph

universalCenterOfMass :: OrbitObject
universalCenterOfMass = OrbitObject "COM"

require :: Bool -> String -> a -> a
require cond msg x = if cond then x else error msg

countDirectAndIndirectOrbits :: OrbitGraph -> Int
countDirectAndIndirectOrbits g = fst $ go S.empty 0 (S.singleton universalCenterOfMass) where
  go visited depth = S.foldr
    (\satellite (count, visited') ->
      require (not $ S.member satellite visited') "OrbitGraph must be a tree" $
        let visited'' = S.insert satellite visited
            (childCount, childVisited) = go visited'' (depth + 1) $ satellites satellite g
        in (count + depth + childCount, childVisited))
    (0, visited)

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn sep = List.unfoldr nextChunk where
  nextChunk [] = Nothing
  nextChunk xs = Just $
    let (word, rest) = List.break (==sep) xs
    in (word, List.dropWhile (==sep) rest)

readOrbitLine :: String -> OrbitGraph -> OrbitGraph
readOrbitLine line =
  let [center, satellite] = map OrbitObject $ splitOn ')' line
  in addOrbit center satellite

compose :: [(a -> a)] -> a -> a
compose = foldr (.) id

main :: IO ()
main = interact
  $ show
  . countDirectAndIndirectOrbits
  . ($ emptyOrbitGraph)
  . compose
  . map readOrbitLine
  . lines
