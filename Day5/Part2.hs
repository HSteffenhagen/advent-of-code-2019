{-# LANGUAGE BangPatterns #-}
module Part2
where

import qualified Data.IntMap as M
import Control.Monad (when)
import Control.Monad.State
import qualified Data.List as List
import System.Exit(exitSuccess)
import Control.Monad(forM_)
import Text.Printf(printf)
import Control.Monad.IO.Class(liftIO)
import System.IO(hPutStrLn, stderr)

data MachineState = MachineState
  { machineMemory :: M.IntMap Int,
    machineCursor :: Int,
    machineInputs :: [Int]
  }

instance Show MachineState where
  show s = unlines
    . map (\(k,v) -> show k ++ ": " ++ show v ++ if k == machineCursor s then " <-" else "")
    . M.toList
    $ machineMemory s

type MachineM = StateT MachineState IO

debug :: String -> MachineM ()
debug msg = liftIO $ do
  hPutStrLn stderr "<<< DEBUG"
  hPutStrLn stderr  msg
  hPutStrLn stderr "<<< END"

readMemory :: Int -> MachineM Int
readMemory address = gets $ \s -> case M.lookup address (machineMemory s) of
  Just value -> value
  Nothing    -> error 
    $ "readMemory "
    ++ show address
    ++ " ("
    ++ show s
    ++ ") : Out of bounds memory access"

writeMemory :: Int -> Int -> MachineM ()
writeMemory address value = modify $ \s -> s
  { machineMemory = M.insert address value $ machineMemory s }

readNext :: MachineM Int
readNext = state go where
  go s = (nextValue, s {machineCursor = machineCursor s + 1}) where
    nextValue = case M.lookup (machineCursor s) (machineMemory s) of
      Just value -> value
      Nothing    -> error
        $ "readNext (" ++ show s ++ "): Cursor out of bounds"

readInitialMemory :: String -> M.IntMap Int
readInitialMemory = M.fromList . zip [0..] . map read . splitOn ',' where
  splitOn sep = List.unfoldr nextWord where
    nextWord [] = Nothing
    nextWord str = let (word, rest) = break (== ',') str
      in Just (word, dropWhile (==',') rest)

readInput :: MachineM Int
readInput = do
  (input:inputs) <- gets machineInputs
  modify (\s -> s {machineInputs = inputs})
  return input

data Opcode
  = Plus 
  | Multiply 
  | Input 
  | Output 
  | JumpIfTrue
  | JumpIfFalse
  | LessThan
  | Equals
  | Halt
  deriving (Eq, Show)

data OperandMode = Position | Immediate
  deriving Show
data Operation = Operation Opcode [OperandMode]
  deriving Show

readOperand :: OperandMode -> MachineM Int
readOperand Immediate = readNext
readOperand Position = readNext >>= readMemory

doBinOp :: (Int -> Int -> Int) -> [OperandMode] -> MachineM ()
doBinOp op [lhsMode, rhsMode] = do
  lhs <- readOperand lhsMode
  rhs <- readOperand rhsMode
  target <- readNext
  writeMemory target $ op lhs rhs

doPlus :: [OperandMode] -> MachineM ()
doPlus = doBinOp (+)

doMultiply :: [OperandMode] -> MachineM ()
doMultiply = doBinOp (*)

doGetInput :: [OperandMode] -> MachineM ()
doGetInput [] = do
  input <- readInput
  address <- readNext
  writeMemory address input

doOutput :: [OperandMode] -> MachineM ()
doOutput [opMode] = do
  operand <- readOperand opMode
  liftIO $ print operand

doJumpIfTrue :: [OperandMode] -> MachineM ()
doJumpIfTrue [condMode, targetMode] = do
  cond <- readOperand condMode
  target <- readOperand targetMode
  when (cond /= 0) . modify $ \s -> s { machineCursor = target }

doJumpIfFalse :: [OperandMode] -> MachineM ()
doJumpIfFalse [condMode, targetMode] = do
  cond <- readOperand condMode
  target <- readOperand targetMode
  when (cond == 0) . modify $ \s -> s { machineCursor = target }

doLessThan :: [OperandMode] -> MachineM ()
doLessThan = doBinOp $ \x y -> if x < y then 1 else 0

doEquals :: [OperandMode] -> MachineM ()
doEquals = doBinOp $ \x y -> if x == y then 1 else 0

operatorArity :: Opcode -> Int
operatorArity Plus = 2
operatorArity Multiply = 2
operatorArity Input = 0
operatorArity Output = 1
operatorArity JumpIfTrue = 2
operatorArity JumpIfFalse = 2
operatorArity LessThan = 2
operatorArity Equals = 2
operatorArity Halt = 0

readOperator :: MachineM Operation
readOperator = do
  s <- get
  instruction <- readNext
  let
    !opCode = case instruction `mod` 100 of
      1 -> Plus
      2 -> Multiply
      3 -> Input
      4 -> Output
      5 -> JumpIfTrue
      6 -> JumpIfFalse
      7 -> LessThan
      8 -> Equals
      99 -> Halt
      other -> error $ "Couldn't read instruction ‘" ++ show other ++ "’\n" ++ show s
    readMode '0' = Position
    readMode '1' = Immediate
    !modes = take (operatorArity opCode)
      . (++ repeat Position)
      . map readMode
      . reverse
      . show
      $ div instruction 100
  return $ Operation opCode modes


runToHalt :: MachineM ()
runToHalt = do
  cursor <- gets machineCursor
  op@(Operation opCode operatorModes) <- readOperator
  (case opCode of
    Plus -> doPlus 
    Multiply -> doMultiply 
    Input -> doGetInput 
    Output -> doOutput 
    JumpIfTrue -> doJumpIfTrue
    JumpIfFalse -> doJumpIfFalse
    LessThan -> doLessThan
    Equals -> doEquals
    Halt -> const $ return ()) operatorModes
  when (opCode /= Halt) runToHalt

runIntcode :: [Int] -> M.IntMap Int -> IO ()
runIntcode inputs initialMemory =
  let initialState = MachineState
        { machineCursor = 0
        , machineMemory = initialMemory
        , machineInputs = inputs
        }
  in evalStateT runToHalt initialState

main :: IO ()
main = do
  initialMemory <- readInitialMemory <$> getLine
  runIntcode [5] initialMemory
